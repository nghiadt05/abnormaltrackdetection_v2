%script file to work with trajectory learning data
%
%Data comes from:
%
%B. T. Morris and M. M. Trivedi, "Trajectory Learning for Activity Understanding: Unsupervised, Multilevel, and Long-Term Adaptive Approach," IEEE Trans. Pattern Anal. Mach. Intell., vol. 33, no. 11, pp. 2287-2301, Nov. 2011.
%
%http://cvrr.ucsd.edu/bmorris/datasets/dataset_trajectory_analysis.html
%
%

close all;
clear all;

%select dataset
%dataset = {'I5', 'CROSS', 'OMNI1', 'OMNI2'};

dataset = 'CROSS';

datadir = sprintf('data\\%s', dataset);

%% load image of scene
filename = sprintf('%s\\view.jpg', datadir);
im = imread(filename);


%% load training trajectories
filename = sprintf('%s\\train.mat', datadir);
load(filename);

% view raw training tracks
h=figure;
imshow(im,[]);
hold on;
for i = 1:length(tracks_train)
    plot(tracks_train{i}(1,:), tracks_train{i}(2,:));
end
title('Train Trajectories');

% not all training trajectories have "true" label since this was designed
% for unsupervised clustering
% -1 value for labels_true(i) indicates unknown/unmarked training sample i

% ind_tracks_<xx> indicates trajectories at different points during
% learning process

%ind_tracks_filt_l - indicates tracks after basic filtering
%ind_tracks_clust_l - tracks used for route clustering
%ind_tracks_model_l - tracks used for modeling

% view tracks used for modeling
h=figure;
imshow(im,[]);
hold on;
for i = 1:length(tracks_train)
    if(ind_tracks_model_l(i))
        plot(tracks_train{i}(1,:), tracks_train{i}(2,:));
    end
end
title('Trajectories for Modeling');

fprintf(1, '\\**********************************\n');
fprintf(1, 'Trajectory Learning\n');
fprintf(1, '**********************************/\n');
fprintf(1, 'Number of Tracks = %d\n', length(tracks_train));
fprintf(1, 'Number of Labels = %d\n', sum(labels_train~=-1));
fprintf(1, 'Number of Tracks after simple filtering = %d\n', sum(ind_tracks_filt_l));
fprintf(1, 'Number of Tracks after ROI filtering and before clustering = %d\n', sum(ind_tracks_clust_l));
fprintf(1, 'Number of Tracks after route filtering and before modeling = %d\n', sum(ind_tracks_model_l));


%% load test trajectories
filename = sprintf('%s\\test.mat', datadir);
load(filename);

% need
%1) tracks - {Nx} test tracks
%2) labels - [Nx1] true "lane" label (abnormal have -1 label)
%3) abnormal_offline - [Nx1] track abnormality label
%4) abnormal_online - {Nx1}(1xTi) unusual event label (abnormality status for each track point)
%                     cell is empty for non abnormal tracks

%for datset CROSS, offline abnormality is computed for all test data
%but online abnormality is only on a subset of full dataset

%offline testing (full trajecotory)
fprintf(1, '\\**********************************\n');
fprintf(1, 'Offline Testing\n');
fprintf(1, '**********************************/\n');
fprintf(1, 'Number of Tracks = %d\n', length(tracks));
fprintf(1, 'Number of Good Tracks = %d\n', sum(~abnormal_offline));
fprintf(1, 'Number of Abnormalities = %d\n', sum(abnormal_offline));

%online testing
tracks = tracks(1:step:end);
labels = labels(1:step:end);
abnormal_offline = abnormal_offline(1:step:end);
abnormal_online = abnormal_online(1:step:end);

Npts = sum(cellfun(@(x) size(x,2), tracks));
Nabnpts = sum(cellfun(@sum, abnormal_online));

fprintf(1, '\\**********************************\n');
fprintf(1, 'Online Testing\n');
fprintf(1, '**********************************/\n');
fprintf(1, 'Number of Tracks = %d\n', length(tracks));
fprintf(1, 'Number of Track Points = %d\n', Npts);
fprintf(1, 'Number of Unusual Points = %d\n', Nabnpts);


%% plot abnormality

ind = find(abnormal_offline,1);
if(~isempty(ind))
    h=figure;
    imshow(im,[]);
    hold all;
    plot(tracks{ind}(1,:), tracks{ind}(2,:), 'b');
end


%% plot an unusual action

ind = cellfun(@(x) sum(x)>1, abnormal_online);
ind = find(ind, 1);

if(~isempty(ind))
    h=figure;
    imshow(im,[]);
    hold all;
    plot(tracks{ind}(1,:), tracks{ind}(2,:), 'b');
    ind_bad = logical(abnormal_online{ind});
    plot(tracks{ind}(1,ind_bad), tracks{ind}(2,ind_bad), 'rx', 'linewidth', 2, 'markersize', 10);
end

CVRR Trajectory Analysis Dataset
------------------------


IMPORTANT! - current release only contains CROSS and I5 sets


The CVRR Trajectory Analysis Dataset provides data for benchmarking trajectory clustering algorithms. The full dataset contains 4 different scenes: a simulated intersection, a real highway, and two indoor omni directional camera scenes. The datasets are intended for unsupervised trajectory learning algorithms.  The datasets contain only spatial tracking points (velocity must be inferred).  The application goal is to:

Offline (after completion of tracking = full trajectory):
    i) classify trajectories
   ii) detect abnormal trajectories

Online (decisions made at each timestep)
  iii) predict the final track label
   iv) detect unusual action (determine moment of abnormal event)
   
The dataset is divided into two sections: a) training and b) testing.  The training section is intended for unsupervised learning and so full "truth" labels may not be available.  


Dataset Description
------------------------
CROSS: Simulated four way traffic intersection with various through and turn patterns present. Units are pixels.  Full labels are available for training and testing for both offline and online analysis.  19 Activity Paths

I5: Highway trajectories in both direction of I5 outside of UCSD. Trajectories are obtained by a simple visual tracker. Units are pixels. The true cluster labeling considers only the lane.  Only trajectory labels are available (no abnormality info). 8 Activity Paths

OMNI1: Trajectories of humans walking through a lab captured using an omni-directional camera. Units are pixels.  Natural trajectories collected over 24 hours on a single Saturday without participant knowledge.  Full labels available for training and testing for both offline and online analysis.  7 Activity Paths

OMNI2: Trajectories of humans walking through a lab captured using an omni-directional camera. Units are pixels.  Choreographed trajectories during a 30 minute collection period.  15 Activity Paths



File Description
------------------------
Scene data is provided in separate Matlab .mat files and scene image file.

view.jpg - contains image of the scene

train.mat - training data
-> tracks_train - {Nx1} cell array of N training trajectories.  Each trajectory is a [2xT] array of T [x,y] tracking points.
-> labels_train - [Nx1] array of true training label. -1 entry indicates unknown trajectory label
-> ind_tracks_filt_l - [Nx1] logical array indicates tracks after basic filtering (e.g. minimum size)
-> ind_tracks_clust_l - [Nx1] logical array indicates tracks used for route clustering
-> ind_tracks_model_l - [Nx1] logical array indicates tracks used for modeling

test.mat - testing data
-> tracks - {Nx1} cell array of N testing trajectories
-> labels - [Nx1] array of true training label
-> abnormal_offline - [Nx1] true abnormality label for full trajectory
-> abnormal_online - {Nx1} cell array with unusual action label for each trajectory point.  Each cell is [Tx1] true unusual action label.  Empty cells indicate no unusual actions.


File Structure
------------------------
CVRR_dataset_trajectory_analysis.zip
-> CROSS
-> I5
-> OMNI1
-> OMNI2
tl_release.m - script file to access datasets
README.txt - readme file


Reference
------------------------
Please use the following citation when using the dataset:

B. T. Morris and M. M. Trivedi, "Trajectory Learning for Activity Understanding: Unsupervised, Multilevel, and Long-Term Adaptive Approach," IEEE Trans. Pattern Anal. Mach. Intell., vol. 33, no. 11, pp. 2287-2301, Nov. 2011.
%{
    Dataset related functions
+ Read the datasets from files
+ Train and test data preprocess
%}

%%
clc; close all; clear all;
addpath(genpath('./'));

%% Read the dataset from files
global dataset;
dataset.ClusterDataDir = '../Datasets/CVRR_dataset_trajectory_clustering';
dataset.AnalysisDataDir = '../Datasets/CVRR_dataset_trajectory_analysis_v0/';
dataset.Cluster.Cross = load([dataset.ClusterDataDir,'/cross.mat']);
dataset.Cluster.I5 = load([dataset.ClusterDataDir,'/i5.mat']);
dataset.Analysis.CrossTrain = load([dataset.AnalysisDataDir,'/Cross/train.mat']);
dataset.Analysis.CrossTest = load([dataset.AnalysisDataDir,'/Cross/test.mat']);
dataset.Analysis.CrossImg = imread([dataset.AnalysisDataDir,'/Cross/view.jpg']);
dataset.Analysis.I5Train = load([dataset.AnalysisDataDir,'/I5/train.mat']);
dataset.Analysis.I5Test = load([dataset.AnalysisDataDir,'/I5/test.mat']);
dataset.Analysis.I5Img = imread([dataset.AnalysisDataDir,'/I5/view.jpg']);

%% Options
global opt;
opt.NormLength = 200;
opt.DatasetName = 'Cross';
opt.MinorPOIClusThres = 50;
opt.NumOfPOIClusters = 20;
opt.POIOutlierThres = 0.9;
opt.RefineTrack = false;
opt.POICluserDebug = false;

opt.NumOfTrackClusters = 30;
opt.MinorTrackClusThres = 100;
opt.MalDistScale = 2.9;
opt.TrackClusterDebug = false;

%% Normalized the tracks for all datasets
mNormalizeAllDatasets;

%% Take the interested dataset
if strcmp(opt.DatasetName,'Cross')
    TrainTracks = dataset.Analysis.CrossTrain.tracks;
    TestTracks = dataset.Analysis.CrossTest.tracks;    
    SenceImg = dataset.Analysis.CrossImg;
    Labels = dataset.Analysis.CrossTest.abnormal_offline;
elseif strcmp(opt.DatasetName,'I5')
end

%% POI clustering
[ filteredTracks, brokenTrackIdx ] = POIClustering( TestTracks );

%% From the POI filtered tracks, perform trajectory clustering process (unsupervised learning algorithm)
[ trackFeatures, filterClusTracks ] = mTrackClustering( filteredTracks );

%% Train the supervised full-tracjectory model using the full track clusters
fprintf('NumOfAbnTrack = %d\n',length(find(filterClusTracks)));
fprintf('NumOfNorTrack = %d\n',length(find(filterClusTracks==0)));
filterClusTracks = [filterClusTracks';~(filterClusTracks')];
trackFeatures = trackFeatures';
setdemorandstream(491218382);
net = fitnet([10]);
[net,tr] = train(net,trackFeatures,filterClusTracks);

%% Train the supervised online-trajectory model using the full track clusters

%% Test the performance of the full-trajectory model
testX = trackFeatures(:,tr.testInd);
testT = filterClusTracks(:,tr.testInd);
testY = net(testX);
testIndices = vec2ind(testY);
plotconfusion(testT,testY);
[c,cm] = confusion(testT,testY);
fprintf('Percentage Correct Classification   : %f%%\n', 100*(1-c));
fprintf('Percentage Incorrect Classification : %f%%\n', 100*c);
%accuracy = mFullTrackModelTest( net, TestTracks, Labels );












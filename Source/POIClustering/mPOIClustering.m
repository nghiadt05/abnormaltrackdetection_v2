function [ gmfit, X, clusterX ] = mPOIClustering(X, k, POIOutlierThres )
    Sigma = {'diagonal','full'};
    SharedCovariance = {true,false};
    options = statset('MaxIter',1000); % Increase number of EM iterations
    gmfit = fitgmdist(X,k,'CovarianceType',Sigma{1},...
        'SharedCovariance',SharedCovariance{1},'Options',options);
    clusterX = cluster(gmfit,X);
    for i=1:length(X)
        x1 = X(i,1); mu1 = gmfit.mu(clusterX(i),1); sigma1 = gmfit.Sigma(1);
        x2 = X(i,2); mu2 = gmfit.mu(clusterX(i),2); sigma2 = gmfit.Sigma(2);
        pdf = exp(-((x1-mu1)^2/(2*sigma1^2) + (x2-mu2)^2/(2*sigma2^2)));
        %disp(pdf);
        if(pdf<POIOutlierThres)
            clusterX(i) = 0;            
        end
    end
end


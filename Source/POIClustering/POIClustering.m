function [ brokenTrackIdx ] = POIClustering( Tracks )
    global dataset;
    global opt;
    
    %% Form the POIs of all tracks
    for i=1:length(Tracks)
        % POI format: POI(i,:) = [Start_X, Start_Y, End_X, End_Y]
        POI(i,:) = [Tracks{i}(1,1), Tracks{i}(2,1), Tracks{i}(1,end),Tracks{i}(2,end)]; 
    end

    %% Cluster the entrance and exit POIs using MoG then remove outliers POIs
    % cluster the entrace and exit POIs
    InX = POI(:,1:2); OutX = POI(:,3:4);
    [gmfitIn, InX, clusInX] = mPOIClustering(InX,opt.NumOfPOIClusters,opt.POIOutlierThres);
    [gmfitOut, OutX, clusOutX] = mPOIClustering(OutX,opt.NumOfPOIClusters,opt.POIOutlierThres);
    
    % filter the inconfident POIs (far from the Gaussian mean values)
    ZeroClusIdx = unique([find(clusInX==0);find(clusOutX==0)]);
    
    % filter the minor POI clusters (outlier POIs) for the entrance and exit
    % POIs (having non-salient number of tracks)
    if opt.POINonSalientRemoval
        MinorPOIEntrace = [];
        for i=1:opt.NumOfPOIClusters
            tmpClusTrack = find(clusInX==i);
            if(length(tmpClusTrack)<opt.MinorPOIClusThres)
                MinorPOIEntrace = [MinorPOIEntrace;tmpClusTrack];
                clusInX(tmpClusTrack) = 0;
            end
        end

        MinorPOIExit = [];
        for i=1:opt.NumOfPOIClusters
            tmpClusTrack = find(clusOutX==i);
            if(length(tmpClusTrack)<opt.MinorPOIClusThres)
                MinorPOIExit = [MinorPOIExit;tmpClusTrack];
                clusOutX(tmpClusTrack) = 0;
            end
        end

        % remove all broken tracks
        brokenTrackIdx = unique([ZeroClusIdx;MinorPOIEntrace;MinorPOIExit]);
    else
        brokenTrackIdx = ZeroClusIdx;
    end
    
    %% Debugging
    if opt.POICluserDebug
        % Display the entrace POIs clusters after filtering    
        figure('name','Entrace POIs-Filterd');
        imshow(dataset.Analysis.CrossImg);
        hold on;
        gscatter(InX(:,1),InX(:,2),clusInX);
        %plot(gmfitIn.mu(:,1),gmfitIn.mu(:,2),'kx','LineWidth',2,'MarkerSize',10);
        hold off;

        % Display the exit POIs clusters after filtering
        figure('name','Exit POIs-Filterd');
        imshow(dataset.Analysis.CrossImg);
        hold on;
        gscatter(OutX(:,1),OutX(:,2),clusOutX);
        %plot(gmfitOut.mu(:,1),gmfitOut.mu(:,2),'k+','LineWidth',2,'MarkerSize',10);
        hold off;

        % display broken tracks for clustering
        figure('name','Broken tracks');
        imshow(dataset.Analysis.CrossImg);
        hold on;
        for i=1:length(brokenTrackIdx)
            plot(Tracks{brokenTrackIdx(i)}(1,:),Tracks{brokenTrackIdx(i)}(2,:));
        end
        hold off;
% 
%         figure('name','Filtered tracks');
%         imshow(dataset.Analysis.CrossImg);
%         hold on;
%         for i=1:length(filteredTracks)
%             plot(filteredTracks{i}(1,:),filteredTracks{i}(2,:));
%         end
%         hold off;
    end
end


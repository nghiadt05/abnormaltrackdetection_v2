global dataset;
global opt;
%
CrossY0 = size(dataset.Analysis.CrossImg,1);

% % Clustering - Cross dataset
% for i=1:length(dataset.Cluster.Cross.tracks)
%     Track = dataset.Cluster.Cross.tracks(i);
%     TrackX = Track{1}(1,:);
%     TrackY = Track{1}(2,:);
%     TrackX = mTrackNormalize(TrackX,opt.NormLength);
%     TrackY = CrossY0 - mTrackNormalize(TrackY,opt.NormLength);
%     dataset.Cluster.Cross.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
% end
% 
% % Clustering - I5 dataset
% for i=1:length(dataset.Cluster.I5.tracks)
%     Track = dataset.Cluster.I5.tracks(i);
%     TrackX = Track{1}(1,:);
%     TrackY = Track{1}(2,:);
%     TrackX = mTrackNormalize(TrackX,opt.NormLength);
%     TrackY = mTrackNormalize(TrackY,opt.NormLength);
%     dataset.Cluster.I5.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
% end

% Analysis - Cross_Train dataset
% Construct blurring window.
windowWidth = int16(10);
halfWidth = windowWidth / 2;
gaussFilter = gausswin(10);
gaussFilter = gaussFilter / sum(gaussFilter); % Normalize.
for i=1:length(dataset.Analysis.CrossTrain.tracks_train)
    Track = dataset.Analysis.CrossTrain.tracks_train(i);
    TrackX = Track{1}(1,:);
    TrackY = Track{1}(2,:);
    % normalize the track first to increase the datapoints
    TrackX = mTrackNormalize(TrackX,opt.NormLength/2);
    TrackY = mTrackNormalize(TrackY,opt.NormLength/2);
    % apply a filter for the tracks
    TrackX = conv(TrackX, gaussFilter);
    TrackY = conv(TrackY, gaussFilter);    
    % normalize the track first to increase the datapoints
    TrackX = mTrackNormalize(TrackX,opt.NormLength);
    TrackY = mTrackNormalize(TrackY,opt.NormLength);
    dataset.Analysis.CrossTrain.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
end
dataset.Analysis.CrossTrain.labels = dataset.Analysis.CrossTrain.labels_train;
clear dataset.Analysis.CrossTrain.tracks_train;
clear dataset.Analysis.CrossTrain.labels_train;

% Analysis - Cross_Test dataset
for i=1:length(dataset.Analysis.CrossTest.tracks)
    Track = dataset.Analysis.CrossTest.tracks(i);
    TrackX = Track{1}(1,:);
    TrackY = Track{1}(2,:);
    % normalize the track first to increase the datapoints
    TrackX = mTrackNormalize(TrackX,opt.NormLength);
    TrackY = mTrackNormalize(TrackY,opt.NormLength);
    % remove negative positions of each tracks
    if opt.RefineTrack
        negIdxX = find(TrackX<0);
        negIdxY = find(TrackY<0);
        oveIdxX = find(TrackX>475);
        oveIdxY = find(TrackY>356);
        ErrorIdx = unique([negIdxX,negIdxY,oveIdxX,oveIdxY]);
        %remove negative positions and renormalize the track if applicable
        if(size(ErrorIdx,2)>0)
            TrackX(ErrorIdx) = [];
            TrackY(ErrorIdx) = [];     
            TrackX = mTrackNormalize(TrackX,opt.NormLength);
            TrackY = mTrackNormalize(TrackY,opt.NormLength);
        end
    end
%     TrackX = filter (opt.GaussWin,1, TrackX);
%     TrackY = filter (opt.GaussWin,1, TrackY);
    dataset.Analysis.CrossTest.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
end

% % Analysis - I5_Train dataset
% for i=1:length(dataset.Analysis.I5Train.tracks_train)
%     Track = dataset.Analysis.I5Train.tracks_train(i);
%     TrackX = Track{1}(1,:);
%     TrackY = Track{1}(2,:);
%     TrackX = mTrackNormalize(TrackX,opt.NormLength);
%     TrackY = mTrackNormalize(TrackY,opt.NormLength);
%     dataset.Analysis.I5Train.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
% end
% dataset.Analysis.I5Train.labels = dataset.Analysis.I5Train.labels_train;
% 
% % Analysis - I5_Test dataset
% for i=1:length(dataset.Analysis.I5Test.tracks)
%     Track = dataset.Analysis.I5Test.tracks(i);
%     TrackX = Track{1}(1,:);
%     TrackY = Track{1}(2,:);
%     TrackX = mTrackNormalize(TrackX,opt.NormLength);
%     TrackY = mTrackNormalize(TrackY,opt.NormLength);
%     dataset.Analysis.I5Test.tracks(i) =  mat2cell([TrackX;TrackY],2,opt.NormLength);
% end
clear Track TrackX TrackY i CrossY0;
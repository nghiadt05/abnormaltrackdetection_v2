function Features =  mTrackFetExtract(Tracks)
    Features = [];
    for i=1:length(Tracks)
        TraceFilt = transpose(Tracks{i});
        % extract position feature        
        meanPos = mean(TraceFilt)/1000;   
        % extract derivative feature
        TraceShiftedRight = [TraceFilt(1,:);TraceFilt(1:(end-1),:)];
        TraceDer = TraceFilt - TraceShiftedRight;    
        TraceDer(1,:) = TraceDer(2,:);
        meanDer = mean(TraceDer);
        meanDer = meanDer/100;
        % extract direction feature
        alpha =  atan(TraceDer(:,2)./TraceDer(:,1));
        meanAlpha = mean(alpha);
        % feture
        Features = [Features;meanPos,meanDer,meanAlpha];
    end
end
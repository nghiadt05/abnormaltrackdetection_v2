%given atrajectory in x and y coordinate, output returns the normalized
%track with a fixed number of points
function [ NormTrack ] = mTrackNormalize( tmpTrack, NormLength )
    curLength = length(tmpTrack);
    assert(curLength>=2);
    % interpolating the short tracks to have the length greater than the
    % normalized value
    while(curLength<NormLength)    
        normArray = [];
        for i=1:(curLength-1)
            normArray(2*i-1) = tmpTrack(i);
            normArray(2*i) = (tmpTrack(i)+tmpTrack(i+1))/2;
        end
        normArray(end+1) = tmpTrack(end);
        tmpTrack = normArray;
        curLength = length(tmpTrack);    
        if(curLength==NormLength)
            break;
        end
    end

    % resample the normalized tracks to make the final length equal to the
    % normalized value
    if(curLength>NormLength)
        step = (curLength/NormLength);
        NormTrack = [];
        for i=1:NormLength
            curIdx = round(i*step);
            if(curIdx>curLength)
                curIdx = curLength;
            end
            NormTrack(i) = tmpTrack(curIdx);
        end
    end
%     figure(1);
%     plot(NormTrack);
%     figure(2);
%     plot(tmpTrack);
end


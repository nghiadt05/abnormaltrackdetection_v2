function [ accuracy ] = mFullTrackModelTest( net, TestTrackFet, TestTrackLabel )
    pred = net(TestTrackFet');
    pred = vec2ind(pred);
    TestTrackLabel = [TestTrackLabel';~(TestTrackLabel')];
    TestTrackLabel = vec2ind(TestTrackLabel);
    accuracy = (1-sum(abs(TestTrackLabel-pred))/length(TestTrackLabel))*100;
    %fprintf('accuracy = %0.3f\n',accuracy);
end


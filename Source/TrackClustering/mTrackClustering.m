function [ filterClusTracks, gmfit ] = mTrackClustering(filteredTracks, Features)
    global dataset;
    global opt;

    % cluster the tracks using MoG
    Sigma = {'diagonal','full'};
    SharedCovariance = {true,false};
    options = statset('MaxIter',1000); % Increase number of EM iterations
    gmfit = fitgmdist(Features,opt.NumOfTrackClusters,'CovarianceType',Sigma{1},...
        'SharedCovariance',SharedCovariance{1},'Options',options);
    clusterTracks = cluster(gmfit,Features);

    % remove the outliers for each trajectory cluster  
    filterClusTracks = clusterTracks;
    nonSalientTrackidx = [];
    mahalDist = mahal(gmfit,Features);
    for i=1:opt.NumOfTrackClusters  
        subTrackIdx = find(clusterTracks==i);
        % if the number of tracks in this cluster is smaller than a determined
        % threshold, then this cluster is not accounted for the learning
        % process
        if(length(subTrackIdx)<opt.MinorTrackClusThres && ~isempty(subTrackIdx))      
            for j=1:length(subTrackIdx)      
                 filterClusTracks(subTrackIdx(j)) = 0; 
            end                       
            nonSalientTrackidx = [nonSalientTrackidx;subTrackIdx];
        % inside each trajectory cluster, find the tracks that are different
        % from the others, which locate far from the gaussian mean      
        else        
            localMinor = [];    
            %threshold = min(1.8*mean(dist(subTrackIdx,i)),0.6*max(dist(subTrackIdx,i)));
            threshold = opt.MalDistScale*mean(mahalDist(subTrackIdx,i));
            for j=1:length(subTrackIdx)          
                tmpdist = mahalDist(subTrackIdx(j),i);
                if(tmpdist<threshold)
                    localMinor(j) = 0;
                else
                    localMinor(j) = 1;
                    filterClusTracks(subTrackIdx(j)) = 0;
                end
            end

            if opt.TrackClusterDebug
                figure('name','Trajectory clustering all');
                imshow(dataset.Analysis.CrossImg);
                hold on;
                for j=1:length(subTrackIdx)       
                      plot(filteredTracks{subTrackIdx(j)}(1,:),filteredTracks{subTrackIdx(j)}(2,:),'Color','b');
                end
                figure('name','Trajectory clustering major');
                imshow(dataset.Analysis.CrossImg);
                hold on;
                for j=1:length(subTrackIdx)
                    if(~localMinor(j))            
                        plot(filteredTracks{subTrackIdx(j)}(1,:),filteredTracks{subTrackIdx(j)}(2,:),'Color','g');
                    end
                end
                figure('name','Trajectory clustering minor');
                imshow(dataset.Analysis.CrossImg);
                hold on;
                for j=1:length(subTrackIdx)
                    if(localMinor(j))            
                        plot(filteredTracks{subTrackIdx(j)}(1,:),filteredTracks{subTrackIdx(j)}(2,:),'Color','r');
                    end
                end                                
            end        
        end
    end

    if opt.TrackClusterDebug              
        figure('name','Non-salient trajectories');
        imshow(dataset.Analysis.CrossImg);
        hold on;
        for j=1:length(nonSalientTrackidx)               
            plot(filteredTracks{nonSalientTrackidx(j)}(1,:),filteredTracks{nonSalientTrackidx(j)}(2,:),'Color','r');            
        end
        hold off;
    end
end


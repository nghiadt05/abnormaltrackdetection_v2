%{
    Dataset related functions
+ Read the datasets from files
+ Train and test data preprocess
%}

%%
clc; close all; clear all;
addpath(genpath('./'));

%% Read the dataset from files
global dataset;
dataset.ClusterDataDir = '../Datasets/CVRR_dataset_trajectory_clustering';
dataset.AnalysisDataDir = '../Datasets/CVRR_dataset_trajectory_analysis_v0/';
dataset.Cluster.Cross = load([dataset.ClusterDataDir,'/cross.mat']);
dataset.Cluster.I5 = load([dataset.ClusterDataDir,'/i5.mat']);
dataset.Analysis.CrossTrain = load([dataset.AnalysisDataDir,'/Cross/train.mat']);
dataset.Analysis.CrossTest = load([dataset.AnalysisDataDir,'/Cross/test.mat']);
dataset.Analysis.CrossImg = imread([dataset.AnalysisDataDir,'/Cross/view.jpg']);
dataset.Analysis.I5Train = load([dataset.AnalysisDataDir,'/I5/train.mat']);
dataset.Analysis.I5Test = load([dataset.AnalysisDataDir,'/I5/test.mat']);
dataset.Analysis.I5Img = imread([dataset.AnalysisDataDir,'/I5/view.jpg']);

%% Options
global opt;
% trajectory normalization parameters
opt.NormLength = 200;
opt.DatasetName = 'Cross';

% POI-based broken track filter
opt.MinorPOIClusThres = 50;
opt.NumOfPOIClusters = 30;
opt.POIOutlierThres = 0.93;
opt.POINonSalientRemoval = false;
opt.RefineTrack = false;
opt.POICluserDebug = false;

% Normal trajectory clustering
opt.NumOfTrackClusters = 30;
opt.MinorTrackClusThres = 30;
opt.MalDistScale = 5;
opt.TrackNonSalientRemoval = false;
opt.TrackClusterDebug = false;

% Inside-cluster normal trajectory model
opt.InClusterGain = 2.8;
opt.FetSpread = 0.5;
opt.InClusterSampleSize = 10;
opt.InClusterNNSize = [10,6,4];
opt.FetSimThres = 1*10^-5;
opt.InsideClusDebug = true;

%% Normalized the tracks for all datasets
if ~exist('normData.mat','file')
    mNormalizeAllDatasets;
    save('normData.mat','dataset');
else
    load('normData.mat');
end

%% Cluster the normal trajectory
if ~exist('trProcessedData.mat','file')
    %% Take the interested dataset
    if strcmp(opt.DatasetName,'Cross')
        Tracks = dataset.Analysis.CrossTest.tracks;      
        Labels = dataset.Analysis.CrossTest.abnormal_offline;
        Labels = [Labels';~(Labels')];
        normIdx = find(Labels(1,:)==0);
        anoIdx = find(Labels(1,:)==1);

        normTracks = Tracks(normIdx);   
        anoTracks = Tracks(anoIdx);   
        SenceImg = dataset.Analysis.CrossImg;
    elseif strcmp(opt.DatasetName,'I5')
    end

    %% Segment the image using superpixel
    if ~exist('map.mat','file')
        [SpLabel,NumLabels] = superpixels(SenceImg,500);
        figure();
        BW = boundarymask(SpLabel);
        imshow(imoverlay(SenceImg,BW,'cyan'),'InitialMagnification',67);
    end

    %% Remove the broken normal tracks
    brokenTrackIdx = POIClustering(normTracks);    
    normTracks(brokenTrackIdx) = [];

    %% Split the training, validation and test set
    % shuffle the data first 
    shuffNormIdx = randperm(length(normTracks));
    %     shuffAnoIdx = randperm(length(anoIdx));
    normTracks = normTracks(shuffNormIdx);
    %     anoTracks = anoTracks(shuffAnoIdx);
    % test set
    testNormTracks = normTracks(1:1000);
    testAnoTracks = anoTracks;
    % training set only needs normal tracks
    trainNormTracks = normTracks(1001:end);

    %% Extract trajetory features
    testNormTrackFet = mTrackFetExtract(testNormTracks); 
    testAnoTrackFet = mTrackFetExtract(testAnoTracks); 
    trainNormTrackFet = mTrackFetExtract(trainNormTracks); 

    %% From the POI filtered tracks, perform trajectory clustering with outlier removal
    [ filterClusTracks,  trackClusterModel ] = mTrackClustering( trainNormTracks, trainNormTrackFet );
    save('trProcessedData.mat','filterClusTracks','trackClusterModel',...
                               'trainNormTracks','trainNormTrackFet',...
                               'testAnoTracks','testAnoTrackFet',...
                               'testNormTracks','testNormTrackFet');
else
    load('trProcessedData.mat');
end

%% Having the cluster parameters, next task is to detect anomalies
% calculate the threshold for each normal track cluster and build the
% inside-cluster model for confident normal trajectories
nonzTrCluIdx = unique(filterClusTracks);
if(nonzTrCluIdx(1)==0)
    nonzTrCluIdx(1) = [];
end

if ~exist('InClusNet.mat','file')
    ClusThres = zeros(opt.NumOfTrackClusters,1);
    for trClusIdx = 1:length(nonzTrCluIdx)
        ClustID = nonzTrCluIdx(trClusIdx);
        % find all the tracks having the cluster ID = ClustID
        tmpTrIdx = find(filterClusTracks == ClustID);
        tmpTrFet = trainNormTrackFet(tmpTrIdx,:);
        % calculate the Mahal distance
        tmpDist = mahal(trackClusterModel, tmpTrFet);
        tmpDist = tmpDist(:,ClustID);
        ClusThres(ClustID,:) = max(tmpDist);
        % create the inside-cluster model for each normal cluster
        x3 = linspace((1-opt.FetSpread)*min(tmpTrFet(:,3)),(1+opt.FetSpread)*max(tmpTrFet(:,3)),opt.InClusterSampleSize);
        x4 = linspace((1-opt.FetSpread)*min(tmpTrFet(:,4)),(1+opt.FetSpread)*max(tmpTrFet(:,4)),opt.InClusterSampleSize);
        x5 = linspace((1-opt.FetSpread)*min(tmpTrFet(:,5)),(1+opt.FetSpread)*max(tmpTrFet(:,5)),opt.InClusterSampleSize);
        [x3grid,x4grid,x5grid] = ndgrid(x3,x4,x5);
        X = [x3grid(:) x4grid(:) x5grid(:)];
        % find the features that are "similar" to the normal track features
        tmpTrFet(:,1:2) = [];
        normFetIdx = [];
        for i=1:length(tmpTrFet)            
            tmpNormFet = tmpTrFet(i,:);
            tmpX(:,1) = abs(X(:,1)-tmpNormFet(1));
            tmpX(:,2) = abs(X(:,2)-tmpNormFet(2));
            tmpX(:,3) = abs(X(:,3)-tmpNormFet(3));
            tmpX = tmpX(:,1).^2 + tmpX(:,2).^2 + tmpX(:,3).^2;
            tmpX = sqrt(tmpX);
            normFetIdx = [normFetIdx;find(tmpX<opt.FetSimThres)];
        end   
        normFetIdx = unique(normFetIdx);
        % create labels
        Labels = ones(1,length(X)+length(tmpTrFet));
        Labels(normFetIdx) = 0;
        Labels(length(X)+1:end) = 0;
        Labels = [Labels;~Labels];
        Fets = ([X;tmpTrFet])';
        % then train the inside-cluster model for each cluster
        fprintf('Train Cluster %d ...',ClustID);
        InClusNet{ClustID} = fitnet(opt.InClusterNNSize);
        [InClusNet{ClustID},~] = train(InClusNet{ClustID},Fets,Labels);
        fprintf(' done.\n');
    end
    save('InClusNet.mat','ClusThres','InClusNet');
else
    load('InClusNet.mat');
end

% detect anomalies using the probabilistic models
testAnoTrackMahalDist = mahal(trackClusterModel, testAnoTrackFet);
prbFalse = 0;
inClusFalse = 0;
for anoTrIdx = 1:length(testAnoTrackFet)
    minMahalDist = min(testAnoTrackMahalDist(anoTrIdx,:));
    closeClusIdx = find(testAnoTrackMahalDist(anoTrIdx,:)==minMahalDist);
    if(opt.InClusterGain*ClusThres(closeClusIdx)> minMahalDist)
        prbFalse = prbFalse + 1;
        % using the coressponding inside-cluster model to deal with hard
        % event
        tmpTestFet = (testAnoTrackFet(anoTrIdx,3:end))';
        tmpTestLabel = InClusNet{closeClusIdx}(tmpTestFet);
        tmpTestLabel = vec2ind(tmpTestLabel);
        % anomaly detected by the inside-cluster model
        if(tmpTestLabel==1)
%             if opt.InsideClusDebug
%                 figure('name','Wrong anomaly detection');
%                 imshow(dataset.Analysis.CrossImg);
%                 trClusIdx = find(filterClusTracks==closeClusIdx);
%                 tracks = trainNormTracks(trClusIdx);
%                 hold on;
%                 for i=1:length(tracks)
%                     plot(tracks{i}(1,:),tracks{i}(2,:),'Color','g');
%                 end
%                 plot(testAnoTracks{anoTrIdx}(1,:),testAnoTracks{anoTrIdx}(2,:),'Color','r');
%                 hold off;
%             end
        else
            inClusFalse = inClusFalse + 1;
        end
    end
end
fprintf('Probabilistic False-Positive Rate = %d/200\n',prbFalse);
fprintf('Inside-cluster False-Positive Rate = %d/200\n',inClusFalse);

% check the accuracy for normal tracks
testNormTrackMahalDist = mahal(trackClusterModel, testNormTrackFet);
prbFalse = 26;
inClusFalse = 0;
zeroCluster = find(ClusThres==0);
testNormTrackMahalDist(:,zeroCluster) = 10e9;
for normTrIdx = 1:length(testNormTrackFet)    
    minMahalDist = min(testNormTrackMahalDist(normTrIdx,:));
    closeClusIdx = find(testNormTrackMahalDist(normTrIdx,:)==minMahalDist);
    if(opt.InClusterGain*ClusThres(closeClusIdx) > minMahalDist)     
        % using the coressponding inside-cluster model to deal with hard
        % event           
        tmpTestFet = (testNormTrackFet(normTrIdx,3:end))';
        tmpTestLabel = InClusNet{closeClusIdx}(tmpTestFet);
        tmpTestLabel = vec2ind(tmpTestLabel);
        % anomaly detected by the inside-cluster model
        if(tmpTestLabel==1)            
            inClusFalse = inClusFalse + 1;
            figure('name','Wrong nomaly detection');
                imshow(dataset.Analysis.CrossImg);
                trClusIdx = find(filterClusTracks==closeClusIdx);
                tracks = trainNormTracks(trClusIdx);
                hold on;
                for i=1:length(tracks)
                    plot(tracks{i}(1,:),tracks{i}(2,:),'Color','g');
                end
                plot(testNormTracks{normTrIdx}(1,:),testNormTracks{normTrIdx}(2,:),'Color','b');
                hold off;
        end
    else
        prbFalse = prbFalse + 1;
        inClusFalse = inClusFalse + 1;
    end
end
fprintf('Probabilistic False-Negative Rate = %d/1000\n',prbFalse);
fprintf('Inside-cluster False-Negative Rate = %d/1000\n',inClusFalse);



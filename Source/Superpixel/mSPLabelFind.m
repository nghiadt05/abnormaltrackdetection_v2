function [spOverlayLabel,nspOverlayLabel] = mSPLabelFind(trainNormTracks,SpLabel, NumLabels, senceSize)
       spOverlayLabel = [];
    for i=1:10:length(trainNormTracks)
        trackTmp = round(trainNormTracks{i});        
        for j=1:2:length(trackTmp)
            % find the superpixel label
            idx = [trackTmp(2,j),trackTmp(1,j)];
            if (idx(1)>0 && idx(2)>0 && idx(1)<senceSize(1) && idx(2)<senceSize(2))
                spOverlayLabel = [spOverlayLabel;SpLabel(idx(1),idx(2))];
            end
        end        
    end
    spOverlayLabel = unique(spOverlayLabel);
    
    nspOverlayLabel = 1:1:NumLabels;    
    for i = 1:1:length(spOverlayLabel)
        idx = find(nspOverlayLabel== spOverlayLabel(i));
        if ~isempty(idx)
            nspOverlayLabel(idx) = [];
        end
    end
    nspOverlayLabel = nspOverlayLabel';
    
%     imIdx = [];
%     for i=1:length(spOverlayLabel)
%         [idxVer, idxHor]  = find(SpLabel==spOverlayLabel(i));
%         imIdx = [imIdx;[idxVer, idxHor]];
%     end
%     
%     figure('name','Overlay SP');
%     testMap = zeros(356,475);
%     for i=1:length(imIdx)
%         testMap(imIdx(i,1),imIdx(i,2)) = 1;
%     end
%     imshow(testMap);
end
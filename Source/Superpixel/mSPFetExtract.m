function [ spLabel, spFeat ] = mSPFetExtract(SenceImg, SpLabel, spOverlayLabel, nspOverlayLabel )
    spLabel = [];
    spFeat = [];
    for i=1:length(spOverlayLabel)
        spIdx = spOverlayLabel(i);
        [pixl.Hor,pixl.Ver]  = find(SpLabel==spIdx);
        RGB = [0,0,0];
        for j=1:length(pixl.Hor)
            tmpRGB = double(SenceImg(pixl.Hor(j),pixl.Ver(j),:));
            RGB = RGB + ([tmpRGB(1,1,1),tmpRGB(1,1,2),tmpRGB(1,1,3)]);
        end
        RGB = RGB./length(pixl.Hor);
        spFeat = [spFeat;[RGB,rgb2gray(RGB),mean(pixl.Hor),mean(pixl.Ver)]];        
        spLabel = [spLabel,[1;0]];
    end
    
    for i=1:length(nspOverlayLabel)
        spIdx = nspOverlayLabel(i);
        [pixl.Hor,pixl.Ver]  = find(SpLabel==spIdx);
        RGB = [0,0,0];
        for j=1:length(pixl.Hor)
            tmpRGB = double(SenceImg(pixl.Hor(j),pixl.Ver(j),:));
            RGB = RGB + ([tmpRGB(1,1,1),tmpRGB(1,1,2),tmpRGB(1,1,3)]);
        end
        RGB = RGB./length(pixl.Hor);
        spFeat = [spFeat;[RGB,rgb2gray(RGB),mean(pixl.Hor),mean(pixl.Ver)]];        
        spLabel = [spLabel,[0;1]];
    end
end


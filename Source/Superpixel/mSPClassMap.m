function map = mSPClassMap(SenceImg, SpLabel, NumLabels, spnet)    
    map = zeros(size(SpLabel));   
    for spIdx=1:NumLabels      
        [pixl.Hor,pixl.Ver] = find(SpLabel==spIdx);
        RGB = [0,0,0];
        for j=1:length(pixl.Hor)
            tmpRGB = double(SenceImg(pixl.Hor(j),pixl.Ver(j),:));
            RGB = RGB + ([tmpRGB(1,1,1),tmpRGB(1,1,2),tmpRGB(1,1,3)]);
        end
        RGB = RGB./length(pixl.Hor);
        tmpFeat = [RGB,rgb2gray(RGB),mean(pixl.Hor),mean(pixl.Ver)]; 
        tmpLabel = vec2ind(spnet(tmpFeat'));
        if tmpLabel == 1
            for j=1:length(pixl.Hor)
                map(pixl.Hor(j),pixl.Ver(j)) = 0.5;
            end
        else
            for j=1:length(pixl.Hor)
                map(pixl.Hor(j),pixl.Ver(j)) = 0;
            end
        end
    end
    figure('name','map');
    imshow(map);
end